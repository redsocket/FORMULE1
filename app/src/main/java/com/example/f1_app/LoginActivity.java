package com.example.f1_app;

import static android.content.ContentValues.TAG;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.f1_app.ui.utils.Constants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {



    private EditText eName;
    private EditText ePassword;
    private Button ebtnRegister;
    private Button ebtnLogin;

    private FirebaseAuth mAuth;
    private FirebaseAuth mAuth1;

    @Override
    protected void onCreate( Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        eName = findViewById(R.id.etName);
        ePassword = findViewById(R.id.etPassword);
        ebtnRegister = findViewById(R.id.btnRegister);
        ebtnLogin = findViewById(R.id.buttonLogin);


        // ...
        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        mAuth1 = FirebaseAuth.getInstance();

        //ebtnLogin

        ebtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String inputName = eName.getText().toString().trim();
                String inputPassword= ePassword.getText().toString().trim();

                Register(inputName,inputPassword);
            }

        });

        ebtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String inputName = eName.getText().toString().trim();
                String inputPassword= ePassword.getText().toString().trim();

                //Login(inputName,inputPassword);
                Login2(inputName,inputPassword); //raccourcis
            }

        });


    }


    @Override
    public void onStart(){
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        FirebaseUser currentUser1 = mAuth1.getCurrentUser();
        if(currentUser != null){
             //reload();
        }
        if(currentUser1 != null){
            // reload();
        }
    }


    public void Register(String inputName,String inputPassword) {
        mAuth.createUserWithEmailAndPassword(inputName, inputPassword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information

                            Log.d(TAG, "createUserWithEmail:success");
                            Toast.makeText(LoginActivity.this,"Succesful",Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(LoginActivity.this, NavActivity.class);
                            final Bundle extras = new Bundle();
                            extras.putString(Constants.Login.EXTRA_LOGIN, eName.getText().toString());
                            intent.putExtras(extras);
                            startActivity(intent);
                            FirebaseUser user = mAuth.getCurrentUser();
                            // updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Already exist or wrong email/password.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }
                    }
                });
    }


    public void Login2(String inputName,String inputPassword) {

        if(inputName.isEmpty() || inputPassword.isEmpty() /* inputName.length()<=6|| inputPassword.length()<=4*/){

            Toast.makeText(LoginActivity.this,"Wrong password or login",Toast.LENGTH_SHORT).show();

        }
        else{ Toast.makeText(LoginActivity.this,"Succesful",Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(LoginActivity.this, NavActivity.class);
            final Bundle extras = new Bundle();
            extras.putString(Constants.Login.EXTRA_LOGIN, eName.getText().toString());
            intent.putExtras(extras);
            startActivity(intent);

        }

    }


    public void Login(String inputName,String inputPassword) {
        mAuth.signInWithEmailAndPassword(inputName, inputPassword)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            Toast.makeText(LoginActivity.this,"Succesful",Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(LoginActivity.this, NavActivity.class);
                            final Bundle extras = new Bundle();
                            extras.putString(Constants.Login.EXTRA_LOGIN, eName.getText().toString());
                            intent.putExtras(extras);
                            startActivity(intent);
                            FirebaseUser user = mAuth.getCurrentUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            // updateUI(null);
                        }
                    }
                });
    }
}