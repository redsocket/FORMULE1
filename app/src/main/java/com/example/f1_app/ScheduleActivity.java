package com.example.f1_app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.databinding.ActivityCalendrierBinding;
import com.example.f1_app.ui.Gestion_Schedule.ScheduleFragment;
import com.example.f1_app.ui.RacesResults.Races;
import com.example.f1_app.ui.RacesResults.Results;
import com.example.f1_app.ui.RacesResults.ResultsRepository;
import com.example.f1_app.ui.RecyclerViewScheduleActivity.MyListScheduleAdapterActivity;
import com.example.f1_app.ui.RecyclerViewScheduleActivity.MyScheduleDataActivity;
import com.example.f1_app.ui.api.Activity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ScheduleActivity extends AppCompatActivity {

    private ActivityCalendrierBinding binding;
    private RecyclerView recyclerView;
    private MyListScheduleAdapterActivity adapter;
    private Button btnclose;
    private Object OnScheduleClick;
    private int adapterPosition;
    private ScheduleFragment ScheduleFragment;




    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


       Intent intent = getIntent();



       int adapterPosition = intent.getIntExtra("roundschedule",1);

       switch (adapterPosition){

           case 1:
               setContentView(R.layout.activity_calendrier);
               callAPI_activity(1);
               callAPIRaces(1);
               break;
           case 2:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(2);
               break;
           case 3:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(3);
               break;
           case 4:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(4);
               break;
           case 5:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(5);
               break;
           case 6:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(6);
               break;
           case 7:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(7);
               break;
           case 8:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(8);
               break;
           case 9:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(9);
               break;
           case 10:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(10);
               break;
           case 11:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(11);
               break;
           case 12:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(12);
               break;
           case 13:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(13);
               break;
           case 14:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(14);
               break;
           case 15:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(15);
               break;
           case 16:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(16);
               break;
           case 17:
               setContentView(R.layout.activity_calendrier);

               callAPI_activity(17);
               break;
           case 18:


               callAPI_activity(18);
               break;
           case 19:
               setContentView(R.layout.comingsoon);
               //callAPI_activity(19);
               break;
           case 20:
               setContentView(R.layout.comingsoon);
               //callAPI_activity(20);
               break;
           case 21:
               setContentView(R.layout.comingsoon);
               //callAPI_activity(21);
               break;
           case 22:
               setContentView(R.layout.comingsoon);
               //callAPI_activity(22);
               break;
       }




        btnclose = (Button) findViewById(R.id.buttondecoactivity);



        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });




    }



    void callAPI_activity(int round){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ergast.com/api/f1/2021/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        
        Activity service = retrofit.create(Activity.class);
        service.listRepos(round).enqueue(new Callback<ResultsRepository>() {
            private String Speed;

            @Override
            public void onResponse(Call<ResultsRepository> call, Response<ResultsRepository> response) {
                ResultsRepository repositories = response.body();
                if (repositories != null){
                    //repositories.toString();


                     Log.d("RESPONSE API2", repositories.toString()); //affichage console




                    List<Results> Results= repositories.getMRData().getRaceTable().getRaces().get(0).getResults();
                    MyScheduleDataActivity[] MyScheduleDataActivity = new MyScheduleDataActivity[Results.size()];


                   // Log.d("round ",Races.get(0).getRound());



                    for(int i=0;i<19;i++){

                        MyScheduleDataActivity[i] = new MyScheduleDataActivity(




                               // Races.get(i).getCircuit().getCircuitName(),
                               // Races.get(i).getCircuit().getLocation().getLocality(),
                                //Races.get(i).getCircuit().getLocation().getCountry(),
                                //Races.get(i).getTime(),
                                //Races.get(i).getDate(),
                                // Races.get(i).getRaceName(),

           Results.get(i).getFastestLap().getAverageSpeed().getUnits(),
                                Results.get(i).getConstructor().getName(),
                                Results.get(i).getDriver().getNationality(),
                                Results.get(i).getDriver().getGivenName(),
                                Results.get(i).getDriver().getFamilyName(),
                                Results.get(i).getFastestLap().getRank(),
                                Results.get(i).getFastestLap().getLap(),
                                Results.get(i).getFastestLap().getTime().getTime(),

                                Results.get(i).getNumber(),
                                Results.get(i).getPosition(),
                                Results.get(i).getPoints(),
                                Results.get(i).getGrid(),
                                Results.get(i).getLaps(),
                 Results.get(i).getFastestLap().getAverageSpeed().getSpeed());
                        //Log.d("speed",Results.get(i).getFastestLap().getAverageSpeed().getSpeed());

                    }


                  // Log.d("time", Results.get(0).getFastestLap().getTime().getTime());


                    recyclerView = findViewById(R.id.activityCalendrier);
                    MyListScheduleAdapterActivity adapter = new MyListScheduleAdapterActivity(MyScheduleDataActivity);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                    recyclerView.setAdapter(adapter);


                    adapter.add3(MyScheduleDataActivity);
                    adapter.notifyDataSetChanged();


                }

                else{
                    Log.d("RESPONSE API2","non ok");
                }
            }

            @Override
            public void onFailure(Call<ResultsRepository> call, Throwable t) {

                Log.e("err",t.toString());

            }
        });


    }

    void callAPIRaces(int round){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ergast.com/api/f1/2021/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        Activity service = retrofit.create(Activity.class);
        service.listRepos(round).enqueue(new Callback<ResultsRepository>() {
            @Override
            public void onResponse(Call<ResultsRepository> call, Response<ResultsRepository> response) {
                ResultsRepository repositories = response.body();
                if (repositories != null){
                    //repositories.toString();


                    Log.d("RESPONSE API2", repositories.toString()); //affichage console

                    List<Races>  Races = repositories.getMRData().getRaceTable().getRaces();

                    Log.d("RESPONSE API2",      Races.get(0).getCircuit().getCircuitName()); //affichage console

                    Races.get(0).getCircuit().getLocation().getLocality();
                    Races.get(0).getCircuit().getLocation().getCountry();
                    Races.get(0).getCircuit().getCircuitName();
                    Races.get(0).getTime();
                    Races.get(0).getDate();
                    Races.get(0).getRaceName();



                }

                else{
                    Log.d("RESPONSE API2","non ok");
                }
            }

            @Override
            public void onFailure(Call<ResultsRepository> call, Throwable t) {

                Log.e("err",t.toString());

            }
        });


    }

}
