package com.example.f1_app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.ui.DriversInformations.Drivers;
import com.example.f1_app.ui.DriversInformations.DriversRepository;
import com.example.f1_app.ui.RecyclerView_DriversInformations.MyListAdapterDriversInformations;
import com.example.f1_app.ui.RecyclerView_DriversInformations.MyListDataDriversInformations;
import com.example.f1_app.ui.api.DriverInformation;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
public class PilotesActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Button boutonclose;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();



        int adapterPosition = intent.getIntExtra("Pilotes",1);

        switch (adapterPosition) {

            case 1:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("alonso");

                break;
            case 2:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("bottas");

                break;
            case 3:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("gasly");

                break;
            case 4:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("giovinazzi");

                break;
            case 5:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("hamilton");

                break;
            case 6:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("kubica");

                break;
            case 7:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("latifi");

                break;
            case 8:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("leclerc");

                break;
            case 9:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("mazepin");

                break;
            case 10:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("norris");

                break;
            case 11:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("ocon");

                break;
            case 12:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("pérez");
                break;
            case 13:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("räikkönen");

                break;
            case 14:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("ricciardo");

                break;
            case 15:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("russell");

                break;
            case 16:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("sainz");

                break;
            case 17:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("mick_schumacher");

                break;
            case 18:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("stroll");

                break;
            case 19:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("tsunoda");

                break;
            case 20:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("max_verstappen");

                break;
            case 21:
                setContentView((R.layout.activity_pilotes));
                callAPI_DriversIndormations("vettel");

                break;

        }

        boutonclose = (Button) findViewById(R.id.pilotesActivity);

        boutonclose.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View view)
            {
                finish();
            }

        });

    }


void callAPI_DriversIndormations(String driverId){

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://ergast.com/api/f1/drivers/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            DriverInformation service = retrofit.create(DriverInformation.class);
            service.listRepos(driverId).enqueue(new Callback<DriversRepository>() {
                @Override
                public void onResponse(Call<DriversRepository> call, Response<DriversRepository> response) {
                    DriversRepository repository = response.body();

                    if (repository != null) {
                        Log.d("RESPONSE API2", repository.toString()); //affichage console
                    }

                    List<Drivers> Drivers = repository.getMRData().getDriverTable().getDrivers();
                    MyListDataDriversInformations[] MyListDataDriversInformations = new MyListDataDriversInformations[Drivers.size()];


                    for (int i = 0; i < Drivers.size(); i++) {

                        MyListDataDriversInformations[i] = new MyListDataDriversInformations(

                                Drivers.get(i).getDriverId(),
                                Drivers.get(i).getPermanentNumber(),
                                Drivers.get(i).getCode(),
                                Drivers.get(i).getGivenName(),
                                Drivers.get(i).getFamilyName(),
                                Drivers.get(i).getDateOfBirth(),
                                Drivers.get(i).getNationality());

                    }
                    recyclerView = findViewById(R.id.activity_pilotes);
                    MyListAdapterDriversInformations adapter = new MyListAdapterDriversInformations(MyListDataDriversInformations);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                    recyclerView.setAdapter(adapter);


                    adapter.add(MyListDataDriversInformations);
                    adapter.notifyDataSetChanged();


                }



                @Override
                public void onFailure(Call<DriversRepository> call, Throwable t) {

                }
            });

    }










}


