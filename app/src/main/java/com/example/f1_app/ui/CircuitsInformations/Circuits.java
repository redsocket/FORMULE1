package com.example.f1_app.ui.CircuitsInformations;

public class Circuits {

    private String circuitId;
    private String circuitName;

    private Location Location;

    public String getCircuitId() {
        return circuitId;
    }

    public void setCircuitId(String circuitId) {
        this.circuitId = circuitId;
    }

    public String getCircuitName() {
        return circuitName;
    }

    public void setCircuitName(String circuitName) {
        this.circuitName = circuitName;
    }

    public com.example.f1_app.ui.CircuitsInformations.Location getLocation() {
        return Location;
    }

    public void setLocation(com.example.f1_app.ui.CircuitsInformations.Location location) {
        Location = location;
    }
}
