package com.example.f1_app.ui.RecyclerViewScheduleActivity;

public class MyScheduleDataActivity {

    private String units;
    private String speed;
    private String circuitId;
    private String circuitName;
    private String lat;
    private String locality;
    private String country;
    private String constructorId;
    private String name;
    private String nationality;
    private String driverId;
    private String permanentNumber;
    private String code;
    private String givenName;
    private String url;
    private String familyName;
    private String dateOfBirth;
    private String rank;
    private String lap;
    private com.example.f1_app.ui.RacesResults.AverageSpeed AverageSpeed;
    private String Time;
    private String date;
    private String time;
    private String season;
    private String round;
    private String raceName;
    private String number;
    private String position;
    private String positionText;
    private String points;
    private String grid;
    private String laps;
    private String Status;

    public MyScheduleDataActivity(
                                  String units,
                                  String name, String nationality,
                                  String givenName, String familyName,
                                  String rank, String lap,
                                  String Time,
                                  String number, String position,
                                  String points, String grid, String laps,
                                  String speed)
    {
        this.units=units;
        this.speed = speed;
        //this.circuitId = circuitId;
        this.circuitName = circuitName;
        //this.locality = locality;
        //this.country = country;
        this.name = name;
        this.nationality = nationality;
        this.givenName = givenName;
        this.familyName = familyName;
        this.rank = rank;
        this.lap = lap;
        //this.time = time;
        //this.date = date;
        //this.raceName = raceName;
        this.Time = Time;
        this.number = number;
        this.position = position;
        this.points = points;
        this.grid = grid;
        this.laps = laps;


    }


    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getCircuitId() {
        return circuitId;
    }

    public void setCircuitId(String circuitId) {
        this.circuitId = circuitId;
    }

    public String getCircuitName() {
        return circuitName;
    }

    public void setCircuitName(String circuitName) {
        this.circuitName = circuitName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getConstructorId() {
        return constructorId;
    }

    public void setConstructorId(String constructorId) {
        this.constructorId = constructorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getPermanentNumber() {
        return permanentNumber;
    }

    public void setPermanentNumber(String permanentNumber) {
        this.permanentNumber = permanentNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getLap() {
        return lap;
    }

    public void setLap(String lap) {
        this.lap = lap;
    }

    public com.example.f1_app.ui.RacesResults.AverageSpeed getAverageSpeed() {

        return AverageSpeed;
    }

    public void setAverageSpeed(com.example.f1_app.ui.RacesResults.AverageSpeed averageSpeed) {

        AverageSpeed = averageSpeed;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getRaceName() {
        return raceName;
    }

    public void setRaceName(String raceName) {
        this.raceName = raceName;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionText() {
        return positionText;
    }

    public void setPositionText(String positionText) {
        this.positionText = positionText;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public String getLaps() {
        return laps;
    }

    public void setLaps(String laps) {
        this.laps = laps;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

   /*public void setTime(com.example.f1_app.ui.RacesResults.Time time) {
        Time = time;
    }*/

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
