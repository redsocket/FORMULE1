package com.example.f1_app.ui.Actuel_classement;

import androidx.annotation.NonNull;

import org.xmlpull.v1.sax2.Driver;

import java.util.List;

public class StandingsLists {

    private String season;
    private String round;
    private List<DriverStandings> DriverStandings;


    @NonNull
    @Override
    public String toString() {
        return
                "" +DriverStandings ;
    }

    public  String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public List<com.example.f1_app.ui.Actuel_classement.DriverStandings> getDriverStandings() {
        return DriverStandings;
    }

    public void setDriverStandings(List<com.example.f1_app.ui.Actuel_classement.DriverStandings> driverStandings) {
        DriverStandings = driverStandings;
    }
}
