package com.example.f1_app.ui.api;



import com.example.f1_app.ui.Schedule.ScheduleRepository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ScheduleSeason {


    @GET("{user}/2021.json")
    Call<ScheduleRepository> listRepos(@Path("user") String user);

}
