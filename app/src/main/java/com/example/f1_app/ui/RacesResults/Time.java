package com.example.f1_app.ui.RacesResults;

public class Time {

    private String time;

    @Override
    public String toString() {
        return "Time{" +
                "time='" + time + '\'' +
                '}';
    }

    public String getTime() {

        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
