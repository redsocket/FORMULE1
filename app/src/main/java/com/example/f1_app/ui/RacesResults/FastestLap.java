package com.example.f1_app.ui.RacesResults;

public class FastestLap {

    private String rank;
    private String lap;
    private AverageSpeed AverageSpeed;
    private Time Time;

    @Override
    public String toString() {
        return "FastestLap{" +
                "rank='" + rank + '\'' +
                ", lap='" + lap + '\'' +
                ", AverageSpeed=" + AverageSpeed +
                ", Time=" + Time +
                '}';
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getLap() {
        return lap;
    }

    public void setLap(String lap) {
        this.lap = lap;
    }

    public com.example.f1_app.ui.RacesResults.AverageSpeed getAverageSpeed() {
        return AverageSpeed;
    }

    public void setAverageSpeed(com.example.f1_app.ui.RacesResults.AverageSpeed averageSpeed) {
        AverageSpeed = averageSpeed;
    }

    public com.example.f1_app.ui.RacesResults.Time getTime() {
        return Time;
    }

    public void setTime(com.example.f1_app.ui.RacesResults.Time time) {
        Time = time;
    }
}
