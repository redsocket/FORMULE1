package com.example.f1_app.ui.Actuel_classement;

import androidx.annotation.NonNull;

import java.util.List;

public class MRData {
    private String series;
    private String total;
    private StandingsTable StandingsTable;


    @NonNull
    @Override
    public String toString() {
        return "MRData{" +
                "series='" + series + '\n' +
                 "total=" + total +'\n'+
                ", StandingsTable=" + StandingsTable +
                '}';
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public com.example.f1_app.ui.Actuel_classement.StandingsTable getStandingsTable() {
        return StandingsTable;
    }

    public void setStandingsTable(com.example.f1_app.ui.Actuel_classement.StandingsTable standingsTable) {
        StandingsTable = standingsTable;
    }
}
