package com.example.f1_app.ui.RecyclerViewCircuits;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.R;
import com.example.f1_app.ui.Gestion_InfosPage.FragmentInfos.PlaceHolderInfo;
import com.squareup.picasso.Picasso;

public class MyListAdapterCircuits extends RecyclerView.Adapter<MyListAdapterCircuits.ViewHolder> {

    private MyCircuitData[] listdata;
    private int size;

    private PlaceHolderInfo onCircuitsListener;

    public MyListAdapterCircuits(MyCircuitData[] listdata, PlaceHolderInfo onCircuitsListener) {

        if (this.listdata != null) {

            this.listdata = listdata;
            this.size = listdata.length;
            ;
        } else {
            this.size = 0;
        }

        this.onCircuitsListener=onCircuitsListener;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_items_circuits, parent, false);
        MyListAdapterCircuits.ViewHolder viewHolder = new MyListAdapterCircuits.ViewHolder(listItem, this.onCircuitsListener);
        return viewHolder;
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        final MyCircuitData MyCircuitData = listdata[position];
        holder.textView.setText(listdata[position].getCircuitName());
        holder.textView1.setText((CharSequence) listdata[position].getCountry());
        holder.textView2.setText((CharSequence) listdata[position].getLongueur());
        holder.textView3.setText((CharSequence) listdata[position].getLat());
        holder.textView4.setText((CharSequence) listdata[position].getLocality());
        holder.textView5.setText((CharSequence) listdata[position].getCircuitName());

        if(listdata[position].getLocality().equals("Austin")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/USA%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Sakhir")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Bahrain%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Baku")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Azerbaijan%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Montmeló")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Spain%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Budapest")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Hungar%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Imola")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Emilia%20Romagna%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("São Paulo")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Brazil%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Istanbul")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Turkey%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Jeddah")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Saudi%20Arabia%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Al Daayen")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Qatar%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Monte-Carlo")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Monte%20Carlo%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Monza")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Italy%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Portimão")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Portugal%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Spielberg")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Styria%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Le Castellet")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/France%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Mexico City")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Mexico%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Silverstone")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Great%20Britain%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Sochi")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Russi%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Spa")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Belgium%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Abu Dhabi")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Abu%20Dhab%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }
        if(listdata[position].getLocality().equals("Zandvoort")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/2018-redesign-assets/Track%20icons%204x3/Netherlands%20carbon.png.transform/3col/image.png").into(holder.imageView11);
        }


        if(listdata[position].getCountry().equals("USA")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Flag_of_the_United_States.svg/800px-Flag_of_the_United_States.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Bahrain")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Flag_of_Bahrain.svg/800px-Flag_of_Bahrain.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Azerbaijan")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/800px-Flag_of_Azerbaijan.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Spain")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/750px-Flag_of_Spain.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Hungary")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c1/Flag_of_Hungary.svg/800px-Flag_of_Hungary.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Brazil")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Flag_of_Brazil.svg/720px-Flag_of_Brazil.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Turkey")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/b/b4/Flag_of_Turkey.svg/800px-Flag_of_Turkey.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Saudi Arabia")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Flag_of_Saudi_Arabia.svg/800px-Flag_of_Saudi_Arabia.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Qatar")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Flag_of_Qatar.svg/800px-Flag_of_Qatar.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Monaco")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Flag_of_Monaco.svg/750px-Flag_of_Monaco.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Italy")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/800px-Flag_of_Italy.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Portugal")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Portugal.svg/600px-Flag_of_Portugal.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Austria")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/4/41/Flag_of_Austria.svg/1024px-Flag_of_Austria.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("France")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/langfr-1280px-Flag_of_France.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Mexico")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/800px-Flag_of_Mexico.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("UK")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/800px-Flag_of_the_United_Kingdom.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Russia")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/800px-Flag_of_Russia.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Belgium")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Flag_of_Belgium_%28civil%29.svg/800px-Flag_of_Belgium_%28civil%29.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("UAE")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/c/cb/Flag_of_the_United_Arab_Emirates.svg/800px-Flag_of_the_United_Arab_Emirates.svg.png").into(holder.imageView10);
        }
        else if(listdata[position].getCountry().equals("Netherlands")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/800px-Flag_of_the_Netherlands.svg.png").into(holder.imageView10);
        }

    }


    public void add1(MyCircuitData[] data_circuits){

        this.listdata=data_circuits;
        this.size=data_circuits.length;
    }



    public int getItemCount() {
        return this.size;
    }

    public interface OnCircuitsListener{

        void OnCircuitsClick(int adapterPosition);
    }
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView textView;
        public TextView textView1;
        public TextView textView2;
        public TextView textView3;
        public TextView textView4;
        public TextView textView5;
        public ImageView imageView10;
        public ImageView imageView11;
        public RelativeLayout relativeLayout;

        private OnCircuitsListener onCircuitsListener;




        public ViewHolder(View itemView, OnCircuitsListener onCircuitsListener) {
            super(itemView);

            this.textView = (TextView) itemView.findViewById(R.id.locality);
            this.textView1 = (TextView) itemView.findViewById(R.id.country);
            this.textView2 = (TextView) itemView.findViewById(R.id.longuitude);
            this.textView3 = (TextView) itemView.findViewById(R.id.latitude);
            this.textView4 = (TextView) itemView.findViewById(R.id.locality);
            this.textView5 = (TextView) itemView.findViewById(R.id.circuitName);

            this.imageView10 = (ImageView) itemView.findViewById(R.id.imageView10);
            this.imageView11 = (ImageView) itemView.findViewById(R.id.imageView11);

            relativeLayout = (RelativeLayout)itemView.findViewById((R.id.ResultView));
            itemView.setOnClickListener(this);
            this.onCircuitsListener=onCircuitsListener;

        }

        @Override
        public  void onClick(View view)
        {
            this.onCircuitsListener.OnCircuitsClick(getAdapterPosition());

        }



    }
}
