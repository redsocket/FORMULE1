package com.example.f1_app.ui.classe_pilotes;

import androidx.annotation.NonNull;

public class Drivers {


    private String driverId;
    private String permanentNumber;
    private String code;
    private String givenName;
    private String url;
    private String familyName;
    private String dateOfBirth;
    private String nationality;


    @NonNull
    @Override
    public String toString() {


        return "\n{" +
                "Nom ="  + familyName +"\n" +
                "Prenom ="  + givenName +"\n" +
                "Numero =" + permanentNumber +"\n" +
                "anniversaire ="  + dateOfBirth +"\n" +
                "nationalite =" + nationality + "\n"+
                "}\n\n";

    }


    public Drivers(){ };

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getPermanentNumber() {
        return permanentNumber;
    }

    public void setPermanentNumber(String permanentNumber) {
        this.permanentNumber = permanentNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }
}
