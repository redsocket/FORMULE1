package com.example.f1_app.ui.Schedule;

public class Circuit {

    private String circuitName;
    private Location Location;

    @Override
    public String toString() {
        return "Circuit{" +
                "circuitName='" + circuitName + '\'' +
                ", Location=" + Location +
                '}';
    }

    public String getCircuitName() {
        return circuitName;
    }

    public void setCircuitName(String circuitName) {
        this.circuitName = circuitName;
    }

    public com.example.f1_app.ui.Schedule.Location getLocation() {
        return Location;
    }

    public void setLocation(com.example.f1_app.ui.Schedule.Location location) {
        Location = location;
    }
}
