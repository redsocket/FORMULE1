package com.example.f1_app.ui.Schedule;

public class Races {

    private String date;
    private String time;

    private String season;
    private String round;
    private  Circuit Circuit;

    @Override
    public String toString() {
        return "Races{" +
                "date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", season='" + season + '\'' +
                ", round='" + round + '\'' +
                ""+Circuit+
                '}';
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public com.example.f1_app.ui.Schedule.Circuit getCircuit() {
        return Circuit;
    }
}
