package com.example.f1_app.ui.Gestion_Classement.FragmentClassement;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.f1_app.R;


public class SectionsPagerAdapterClassement extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.tab_saison_2021, R.string.tab_saison_2020,R.string.tab_saison_2019,R.string.tab_saison_2018};
    private final Context mContext;

    public SectionsPagerAdapterClassement(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;

        Log.d("RESPONSE tab", "position change");
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).


        return PlaceHolderClassement.newInstance(position + 1);


    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 2 total pages.
        return 4;
    }


}
