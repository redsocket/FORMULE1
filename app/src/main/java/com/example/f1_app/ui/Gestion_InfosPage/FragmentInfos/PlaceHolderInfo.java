package com.example.f1_app.ui.Gestion_InfosPage.FragmentInfos;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.CircuitsActivity;
import com.example.f1_app.PilotesActivity;
import com.example.f1_app.R;
import com.example.f1_app.databinding.FragmentInfosBinding;

import com.example.f1_app.ui.RecyclerViewCircuits.MyCircuitData;
import com.example.f1_app.ui.RecyclerViewCircuits.MyListAdapterCircuits;

import com.example.f1_app.ui.RecyclerView_drivers.MyListAdapter_drivers;
import com.example.f1_app.ui.RecyclerView_drivers.MyListData_pilotes;
import com.example.f1_app.ui.api.Circuit2021;
import com.example.f1_app.ui.api.GitHubService;
import com.example.f1_app.ui.class_circuits.Circuits;
import com.example.f1_app.ui.class_circuits.circuitRepository;
import com.example.f1_app.ui.classe_pilotes.CodeRepository;
import com.example.f1_app.ui.classe_pilotes.Drivers;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlaceHolderInfo extends Fragment implements MyListAdapter_drivers.OnDriversListener,MyListAdapterCircuits.OnCircuitsListener {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewInfo PageViewInfo ;
    private FragmentInfosBinding binding;

    RecyclerView recyclerView;

    RecyclerView recyclerView1;

    MyListAdapter_drivers adapter;

    List<Circuits>retourCircuits;
    List<Drivers>retourDrivers;

    MyListAdapterCircuits adapter1;
    int index = 1;

    private int adapterPosition;

    private int getAdapterPosition(){return adapterPosition;}

    public static PlaceHolderInfo newInstance(int index) {
        PlaceHolderInfo fragment = new PlaceHolderInfo();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PageViewInfo = new ViewModelProvider(this).get(PageViewInfo.class);

        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }

        PageViewInfo.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        binding = FragmentInfosBinding.inflate(inflater, container, false);
        View root = binding.getRoot();







        if(index==1) {

            recyclerView = root.findViewById(R.id.driversView);
            adapter = new MyListAdapter_drivers(null,this);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
            callAPI_pilotes();
        }

        else if(index==2)
        {
            recyclerView1 = root.findViewById(R.id.CircuitRecyclerView);
            adapter1 = new MyListAdapterCircuits(null, this);
            recyclerView1.setHasFixedSize(true);
            recyclerView1.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView1.setAdapter(adapter1);
            call_API_circuits();
        }
        return root;
    }


    public void OnDriversClick(int adapterPosition)
    {
        this.adapterPosition = adapterPosition;

        View view = getView();
        Intent intent = new Intent(view.getContext(), PilotesActivity.class);

        intent.putExtra("Pilotes", adapterPosition+1);

        Log.d("Pilotes",retourDrivers.get(adapterPosition).getDriverId());

        startActivity(intent);
    }

    public void OnCircuitsClick(int adapterPosition)
    {
        this.adapterPosition = adapterPosition;

        View view = getView();
        Intent intent = new Intent(view.getContext(), CircuitsActivity.class);

        intent.putExtra("Circuits", adapterPosition+1);

        Log.d("Circuits",retourCircuits.get(adapterPosition).getCircuitName());

        startActivity(intent);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


    void callAPI_pilotes(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ergast.com/api/f1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        GitHubService service = retrofit.create(GitHubService.class);
        service.listRepos("2021").enqueue(new Callback<CodeRepository>() {
            @Override
            public void onResponse(Call<CodeRepository> call, Response<CodeRepository> response) {
                CodeRepository repositories = response.body();
                if (repositories != null){


                    // Log.d("RESPONSE API2", repositories.toString()); //affichage console

                    List<Drivers> Drivers= repositories.getMRData().getDriverTable().getDrivers();
                    MyListData_pilotes[] MyListData_pilotes = new MyListData_pilotes[Drivers.size()];

                    for(int i=0;i<Drivers.size();i++){

                        retourDrivers=repositories.getMRData().getDriverTable().getDrivers();
                        MyListData_pilotes[i] = new MyListData_pilotes(
                                Drivers.get(i).getGivenName(),
                                Drivers.get(i).getFamilyName(),
                                Drivers.get(i).getNationality(),
                                Drivers.get(i).getPermanentNumber(),
                                Drivers.get(i).getDateOfBirth()

                        );
                    }


                    adapter.add(MyListData_pilotes);
                    adapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(Call<CodeRepository> call, Throwable t) {

                Log.e("err",t.toString());

            }
        });


    }


    void call_API_circuits(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ergast.com/api/f1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Circuit2021 service = retrofit.create(Circuit2021.class);
        service.listRepos("2021").enqueue(new Callback<circuitRepository>() {
            @Override
            public void onResponse(Call<circuitRepository> call, Response<circuitRepository> response) {
                circuitRepository repositories = response.body();
                if (repositories != null) {


                    // Log.d("RESPONSE API2", repositories.toString()); //affichage console
                    retourCircuits=repositories.getMRData().getCircuitTable().getCircuits();
                    List<Circuits> Circuits = repositories.getMRData().getCircuitTable().getCircuits();
                    MyCircuitData[] MyCircuitData = new MyCircuitData[Circuits.size()];

                    for (int i = 0; i < Circuits.size(); i++) {

                        MyCircuitData[i] = new MyCircuitData(
                                Circuits.get(i).getCircuitName(),
                                Circuits.get(i).getLocation().getCountry(),
                                Circuits.get(i).getLocation().getLongueur(),
                                Circuits.get(i).getLocation().getLat(),
                                Circuits.get(i).getLocation().getLocality());
                    }
                    adapter1.add1(MyCircuitData);
                    adapter1.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<circuitRepository> call, Throwable t) {

                Log.e("err",t.toString());

            }
        });


    }
}