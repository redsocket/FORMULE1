package com.example.f1_app.ui.DriversInformations;

import androidx.annotation.NonNull;

public class DriversRepository {

    private MRData MRData;

    @NonNull
    @Override
    public String toString() {
        return   MRData.toString() ;
    }

    public com.example.f1_app.ui.DriversInformations.MRData getMRData(){return MRData;}

    public void setMRData(com.example.f1_app.ui.DriversInformations.MRData MRData) {
        this.MRData = MRData;
    }
}
