package com.example.f1_app.ui.CircuitsInformations;

import com.google.gson.annotations.SerializedName;

public class Location {


    private String lat;
    private String locality;
    private String country;

    @SerializedName("long")
    private String longueur;






    @Override
    public String toString() {
        return "Location{" +

                ", locality='" + locality + '\'' +
                ", country='" + country + '\'' +
                ", latitude=" + lat +'\''+
                ", longueur=" + longueur +'\''+
                '}';
    }

    public String getLongueur() {
        return longueur;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
