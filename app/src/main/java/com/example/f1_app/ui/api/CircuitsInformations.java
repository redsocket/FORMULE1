package com.example.f1_app.ui.api;

import com.example.f1_app.ui.CircuitsInformations.CircuitsRepository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CircuitsInformations {

    @GET("{user}.json")
    Call<CircuitsRepository> listRepos(@Path("user") String user);

}
