package com.example.f1_app.ui.RacesResults;

import java.util.List;

public class Races {

    private Circuit Circuit;
    private List<Results> Results;

    private String date;
    private String time;
    private String season;
    private String round;
    private String raceName;

    @Override
    public String toString() {
        return "Races{" +
                "Circuit=" + Circuit +
                ", Results=" + Results +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", season='" + season + '\'' +
                ", round='" + round + '\'' +
                ", raceName='" + raceName + '\'' +
                '}';
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getRaceName() {
        return raceName;
    }

    public void setRaceName(String raceName) {
        this.raceName = raceName;
    }

    public com.example.f1_app.ui.RacesResults.Circuit getCircuit() {
        return Circuit;
    }

    public void setCircuit(com.example.f1_app.ui.RacesResults.Circuit circuit) {
        Circuit = circuit;
    }

    public List<com.example.f1_app.ui.RacesResults.Results> getResults() {
        return Results;
    }

    public void setResults(List<com.example.f1_app.ui.RacesResults.Results> results) {
        Results = results;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
