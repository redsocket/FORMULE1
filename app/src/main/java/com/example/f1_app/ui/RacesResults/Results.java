package com.example.f1_app.ui.RacesResults;

public class Results {

    private String number;
    private String position;
    private String positionText;
    private String points;
    private String grid;
    private String laps;
    private String Status;

    private Driver Driver;
    private Constructor Constructor;
    private FastestLap FastestLap;
    private Time Time;


    @Override
    public String toString() {
        return "Results{" +
                "number='" + number + '\'' +
                ", position='" + position + '\'' +
                ", positionText='" + positionText + '\'' +
                ", points='" + points + '\'' +
                ", grid='" + grid + '\'' +
                ", laps='" + laps + '\'' +
                ", Status='" + Status + '\'' +
                ", Driver=" + Driver +
                ", Constructor=" + Constructor +
                ", FastestLap=" + FastestLap +
                ", Time=" + Time +
                '}';
    }

    public com.example.f1_app.ui.RacesResults.Time getTime() {
        return Time;
    }

    public void setTime(com.example.f1_app.ui.RacesResults.Time time) {
        Time = time;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionText() {
        return positionText;
    }

    public void setPositionText(String positionText) {
        this.positionText = positionText;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public String getLaps() {
        return laps;
    }

    public void setLaps(String laps) {
        this.laps = laps;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public com.example.f1_app.ui.RacesResults.Driver getDriver() {
        return Driver;
    }

    public void setDriver(com.example.f1_app.ui.RacesResults.Driver driver) {
        Driver = driver;
    }

    public com.example.f1_app.ui.RacesResults.Constructor getConstructor() {
        return Constructor;
    }

    public void setConstructor(com.example.f1_app.ui.RacesResults.Constructor constructor) {
        Constructor = constructor;
    }

    public com.example.f1_app.ui.RacesResults.FastestLap getFastestLap() {
        return FastestLap;
    }

    public void setFastestLap(com.example.f1_app.ui.RacesResults.FastestLap fastestLap) {
        FastestLap = fastestLap;
    }
}
