package com.example.f1_app.ui.Gestion_InfosPage;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.f1_app.R;
import com.example.f1_app.databinding.FragmentInfosBinding;
import com.example.f1_app.ui.Gestion_InfosPage.FragmentInfos.SectionsPagerAdapterInfo;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

public class InfosFragment extends Fragment {

    private FragmentInfosBinding binding;

    //private SectionsPagerAdapterInfo SectionsPagerAdapterInfo;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentInfosBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        SectionsPagerAdapterInfo sectionsPagerAdapterInfo = new SectionsPagerAdapterInfo(getContext(), getChildFragmentManager());
        Log.d("RESPONSE tab", "position change");
        ViewPager viewPager = binding.viewPager;
        viewPager.setAdapter(sectionsPagerAdapterInfo);
        TabLayout tabs = binding.tabs;
        tabs.setupWithViewPager(viewPager);
        //FloatingActionButton fab = binding.fab;
        TabLayout fab = binding.tabs;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        return root;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


}
