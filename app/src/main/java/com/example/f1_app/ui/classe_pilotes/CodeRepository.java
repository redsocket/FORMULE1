package com.example.f1_app.ui.classe_pilotes;

import androidx.annotation.NonNull;

public class CodeRepository {

    private MRData MRData;

    @NonNull
    @Override
    public String toString() {
        return   MRData.toString() ;
    }

    public com.example.f1_app.ui.classe_pilotes.MRData getMRData(){
        return MRData;
    }

    public void setMRData(com.example.f1_app.ui.classe_pilotes.MRData MRData) {
        this.MRData = MRData;
    }
}
