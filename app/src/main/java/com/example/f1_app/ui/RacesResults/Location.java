package com.example.f1_app.ui.RacesResults;

public class Location {

    private String locality;
    private String country;

    @Override
    public String toString() {
        return "Location{" +
                "locality='" + locality + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
