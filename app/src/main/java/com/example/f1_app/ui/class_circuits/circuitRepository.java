package com.example.f1_app.ui.class_circuits;
import androidx.annotation.NonNull;

public class circuitRepository extends MRData{

    private MRData MRData;

    @NonNull
    @Override
    public String toString() {return MRData.toString();}

    public com.example.f1_app.ui.class_circuits.MRData getMRData() {
        return MRData;
    }

    public void setMRData(com.example.f1_app.ui.class_circuits.MRData MRData) {
        this.MRData = MRData;
    }
}
