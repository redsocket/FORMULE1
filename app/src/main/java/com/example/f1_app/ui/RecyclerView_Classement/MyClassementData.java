package com.example.f1_app.ui.RecyclerView_Classement;

public class MyClassementData {


    private String givenName;
    private String familyName;
    private String nationality;

    private String name; //contructor //ecuries

    private String position ;
    private String points ;
    private String wins ;

    private String season;
    private String round;


    public MyClassementData(String givenName, String familyName, String nationality, String position, String points,String wins,  String name) {
        this.givenName = givenName;
        this.familyName = familyName;
        this.nationality = nationality;
        this.name = name;
        this.position = position;
        this.points = points;
        this.wins = wins;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getWins() {
        return wins;
    }

    public void setWins(String wins) {
        this.wins = wins;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getVictoires() {
        return wins ;
    }

    public void setVictoires(String round) {
        this.wins = wins;
    }
}
