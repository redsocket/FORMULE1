package com.example.f1_app.ui.api;

import com.example.f1_app.ui.Actuel_classement.ClassementRepository;
import com.example.f1_app.ui.classe_pilotes.CodeRepository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Actuel_classement {

    @GET("{user}/18/driverstandings.json")
    Call<ClassementRepository> listRepos(@Path("user") String user);


}
