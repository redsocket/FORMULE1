package com.example.f1_app.ui.RecyclerView_Schedule;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.R;

public class MyListScheduleAdapater extends RecyclerView.Adapter<MyListScheduleAdapater.ViewHolder> {


    private MyScheduleData[] listdata;
    private OnScheduleListener onScheduleListener;
    private int size=0;

    // RecyclerView recyclerView;
    public MyListScheduleAdapater(MyScheduleData[] listdata, OnScheduleListener onScheduleListener) {
        if(listdata!=null){
            this.listdata = listdata;
            this.size=listdata.length;
        }

        else{this.size=0;}

        this.onScheduleListener=onScheduleListener;
    }


    @Override
    public MyListScheduleAdapater.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_items_schedule, parent, false);

        MyListScheduleAdapater.ViewHolder viewHolder = new MyListScheduleAdapater.ViewHolder(listItem,this.onScheduleListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyListScheduleAdapater.ViewHolder holder, int position) {

        final MyScheduleData MyClassementData = listdata[position];
        holder.textView.setText(listdata[position].getDate());
        holder.textView1.setText(listdata[position].getTime());
        holder.textView2.setText(listdata[position].getRound());
        holder.textView3.setText(listdata[position].getLocality());
        holder.textView4.setText(listdata[position].getCountry());
        holder.textView5.setText(listdata[position].getCircuitName());



    }


    public void add(MyScheduleData[] data){
        this.listdata=data;
        this.size = data.length;
    }


    @Override
    public int getItemCount() {
        return this.size;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        // public ImageView imageView;

        public TextView textView;
        public TextView textView1;
        public TextView textView2;
        public TextView textView3;
        public TextView textView4;
        public TextView textView5;

        private OnScheduleListener onScheduleListener;

        public RelativeLayout relativeLayout;


        public ViewHolder(View itemView, OnScheduleListener onScheduleListener) {


            super(itemView);

            this.textView = (TextView) itemView.findViewById(R.id.date);
            this.textView1 = (TextView) itemView.findViewById(R.id.time);
            this.textView2 = (TextView) itemView.findViewById(R.id.round);
            this.textView3 = (TextView) itemView.findViewById(R.id.Locality);
            this.textView4 = (TextView) itemView.findViewById(R.id.country);
            this.textView5= (TextView) itemView.findViewById(R.id.locality);



            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.ResultView);
            itemView.setOnClickListener(this);
            this.onScheduleListener=onScheduleListener;
        }

        @Override
        public void onClick(View view) {
            this.onScheduleListener.OnScheduleClick(getAdapterPosition());
        }
    }

    public interface OnScheduleListener{
        void OnScheduleClick(int adapterPosition);
    }
}
