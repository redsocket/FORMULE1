package com.example.f1_app.ui.Gestion_Classement.FragmentClassement;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.R;

import com.example.f1_app.databinding.FragmentClassementBinding;
import com.example.f1_app.ui.Actuel_classement.ClassementRepository;
import com.example.f1_app.ui.Actuel_classement.DriverStandings;
import com.example.f1_app.ui.RecyclerView_Classement.MyClassementData;
import com.example.f1_app.ui.RecyclerView_Classement.MyListClassementAdapter;

import com.example.f1_app.ui.api.Actuel_classement;
import com.example.f1_app.ui.api.Ancien_classement;
import com.example.f1_app.ui.api.Ancien_classement_2020;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlaceHolderClassement extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private com.example.f1_app.ui.Gestion_Classement.FragmentClassement.PageViewClassement PageViewClassement ;
    private FragmentClassementBinding binding;
    RecyclerView recyclerView;
    MyListClassementAdapter adapter;
    int index = 1;

    public static PlaceHolderClassement newInstance(int index) {
        PlaceHolderClassement fragment = new PlaceHolderClassement();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PageViewClassement = new ViewModelProvider(this).get(PageViewClassement.class);

        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }

        PageViewClassement.setIndex(index);
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        binding = FragmentClassementBinding.inflate(inflater, container, false);
        View root = binding.getRoot();



        recyclerView = root.findViewById(R.id.ClassementView);
        adapter = new MyListClassementAdapter(null);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        if(index==1) {
            callAPI_2021();
        }
        else if (index==2){

            callAPI_2020();
        }
        else if (index==3){

            callAPI_2019();
        }
        else if (index==4){

            callAPI_2018();
        }
        else {return null;};
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    void callAPI_2021(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ergast.com/api/f1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Actuel_classement service = retrofit.create(Actuel_classement.class);
        service.listRepos("2021").enqueue(new Callback<ClassementRepository>() {
            @Override
            public void onResponse(Call<ClassementRepository> call, Response<ClassementRepository> response) {
                ClassementRepository repositories = response.body();
                if (repositories != null){



                    List<DriverStandings> DriversStandings = repositories.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings();
                    // Log.d("RESPONSE tab",repositories.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings().toString()); //affichage console
                    MyClassementData[] MyClassementData = new MyClassementData[DriversStandings.size()];

                    for(int i=0;i<DriversStandings.size();i++){


                        MyClassementData[i] = new MyClassementData( DriversStandings.get(i).getDriver().getGivenName(),
                                DriversStandings.get(i).getDriver().getFamilyName(),

                                DriversStandings.get(i).getDriver().getNationality(),
                                DriversStandings.get(i).getPosition(),
                                DriversStandings.get(i).getPoints(),
                                DriversStandings.get(i).getWins(),
                                DriversStandings.get(i).getConstructors().get(0).getName());
                    }
                    Log.d("RESPONSE tab", "" + MyClassementData.length); //affichage console*/





                    adapter.add(MyClassementData);
                    adapter.notifyDataSetChanged();

                    // Log.d("RESPONSE API2", repositories.toString()); //affichage console
                }
            }

            @Override
            public void onFailure(Call<ClassementRepository> call, Throwable t) {

                Log.e("err",t.toString());

            }
        });


    }
    void callAPI_2020(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ergast.com/api/f1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Ancien_classement_2020 service = retrofit.create(Ancien_classement_2020.class);
        service.listRepos("2020").enqueue(new Callback<ClassementRepository>() {
            @Override
            public void onResponse(Call<ClassementRepository> call, Response<ClassementRepository> response) {
                ClassementRepository repositories = response.body();
                if (repositories != null){



                    List<DriverStandings> DriversStandings = repositories.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings();
                    // Log.d("RESPONSE tab",repositories.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings().toString()); //affichage console
                    MyClassementData[] MyClassementData = new MyClassementData[DriversStandings.size()];

                    for(int i=0;i<DriversStandings.size();i++){


                        MyClassementData[i] = new MyClassementData( DriversStandings.get(i).getDriver().getGivenName(),
                                DriversStandings.get(i).getDriver().getFamilyName(),

                                DriversStandings.get(i).getDriver().getNationality(),
                                DriversStandings.get(i).getPosition(),
                                DriversStandings.get(i).getPoints(),
                                DriversStandings.get(i).getWins(),
                                DriversStandings.get(i).getConstructors().get(0).getName());
                    }
                    Log.d("RESPONSE tab", "" + MyClassementData.length); //affichage console*/





                    adapter.add(MyClassementData);
                    adapter.notifyDataSetChanged();

                    // Log.d("RESPONSE API2", repositories.toString()); //affichage console
                }
            }

            @Override
            public void onFailure(Call<ClassementRepository> call, Throwable t) {

                Log.e("err",t.toString());

            }
        });


    }
    void callAPI_2019(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ergast.com/api/f1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Ancien_classement service = retrofit.create(Ancien_classement.class);
        service.listRepos("2019").enqueue(new Callback<ClassementRepository>() {
            @Override
            public void onResponse(Call<ClassementRepository> call, Response<ClassementRepository> response) {
                ClassementRepository repositories = response.body();
                if (repositories != null){



                    List<DriverStandings> DriversStandings = repositories.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings();
                    // Log.d("RESPONSE tab",repositories.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings().toString()); //affichage console
                    MyClassementData[] MyClassementData = new MyClassementData[DriversStandings.size()];

                    for(int i=0;i<DriversStandings.size();i++){


                        MyClassementData[i] = new MyClassementData( DriversStandings.get(i).getDriver().getGivenName(),
                                DriversStandings.get(i).getDriver().getFamilyName(),

                                DriversStandings.get(i).getDriver().getNationality(),
                                DriversStandings.get(i).getPosition(),
                                DriversStandings.get(i).getPoints(),
                                DriversStandings.get(i).getWins(),
                                DriversStandings.get(i).getConstructors().get(0).getName());
                    }
                    Log.d("RESPONSE tab", "" + MyClassementData.length); //affichage console*/





                    adapter.add(MyClassementData);
                    adapter.notifyDataSetChanged();

                    // Log.d("RESPONSE API2", repositories.toString()); //affichage console
                }
            }

            @Override
            public void onFailure(Call<ClassementRepository> call, Throwable t) {

                Log.e("err",t.toString());

            }
        });


    }
    void callAPI_2018(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ergast.com/api/f1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Ancien_classement service = retrofit.create(Ancien_classement.class);
        service.listRepos("2018").enqueue(new Callback<ClassementRepository>() {
            @Override
            public void onResponse(Call<ClassementRepository> call, Response<ClassementRepository> response) {
                ClassementRepository repositories = response.body();
                if (repositories != null){



                    List<DriverStandings> DriversStandings = repositories.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings();
                    // Log.d("RESPONSE tab",repositories.getMRData().getStandingsTable().getStandingsLists().get(0).getDriverStandings().toString()); //affichage console
                    MyClassementData[] MyClassementData = new MyClassementData[DriversStandings.size()];

                    for(int i=0;i<DriversStandings.size();i++){


                        MyClassementData[i] = new MyClassementData( DriversStandings.get(i).getDriver().getGivenName(),
                                DriversStandings.get(i).getDriver().getFamilyName(),

                                DriversStandings.get(i).getDriver().getNationality(),
                                DriversStandings.get(i).getPosition(),
                                DriversStandings.get(i).getPoints(),
                                DriversStandings.get(i).getWins(),
                                DriversStandings.get(i).getConstructors().get(0).getName());
                    }
                    Log.d("RESPONSE tab", "" + MyClassementData.length); //affichage console*/





                    adapter.add(MyClassementData);
                    adapter.notifyDataSetChanged();

                    // Log.d("RESPONSE API2", repositories.toString()); //affichage console
                }
            }

            @Override
            public void onFailure(Call<ClassementRepository> call, Throwable t) {

                Log.e("err",t.toString());

            }
        });


    }
}
