package com.example.f1_app.ui.RacesResults;

public class MRData {
    private RaceTable RaceTable;
    private String series;


    @Override
    public String toString() {
        return "MRData{" +
                "RaceTable=" + RaceTable +
                ", series='" + series + '\'' +
                '}';
    }

    public com.example.f1_app.ui.RacesResults.RaceTable getRaceTable() {
        return RaceTable;
    }

    public void setRaceTable(com.example.f1_app.ui.RacesResults.RaceTable raceTable) {
        RaceTable = raceTable;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }
}
