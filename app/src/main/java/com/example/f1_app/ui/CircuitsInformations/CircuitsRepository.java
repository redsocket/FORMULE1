package com.example.f1_app.ui.CircuitsInformations;

import androidx.annotation.NonNull;

public class CircuitsRepository {

    private MRData MRData;


    @NonNull
    @Override
    public String toString() {
        return   MRData.toString() ;
    }

    public com.example.f1_app.ui.CircuitsInformations.MRData getMRData() {
        return MRData;
    }

    public void setMRData(com.example.f1_app.ui.CircuitsInformations.MRData MRData) {
        this.MRData = MRData;
    }
}
