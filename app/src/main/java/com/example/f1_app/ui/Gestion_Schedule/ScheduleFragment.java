package com.example.f1_app.ui.Gestion_Schedule;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.R;
import com.example.f1_app.ScheduleActivity;


import com.example.f1_app.databinding.FragmentScheduleBinding;
import com.example.f1_app.ui.RecyclerView_Schedule.MyListScheduleAdapater;
import com.example.f1_app.ui.RecyclerView_Schedule.MyScheduleData;
import com.example.f1_app.ui.Schedule.Races;
import com.example.f1_app.ui.Schedule.ScheduleRepository;
import com.example.f1_app.ui.api.ScheduleSeason;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ScheduleFragment extends Fragment implements MyListScheduleAdapater.OnScheduleListener{

    //private HomeViewModel homeViewModel;
    private FragmentScheduleBinding binding;
    private MyListScheduleAdapater adapter;
    private RecyclerView recyclerView;
    List<Races> retourCalendrier;
    private int adapterPosition;


    private int getAdapterPosition() {
        return adapterPosition;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
      //  homeViewModel =
         //       new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentScheduleBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        recyclerView = (RecyclerView) root.findViewById(R.id.ScheduleView);
        adapter = new MyListScheduleAdapater(null, this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        callAPI2();

        return root;
    }


    public void OnScheduleClick (int adapterPosition){
        this.adapterPosition = adapterPosition;

        View view = getView();
        Intent intent = new Intent(view.getContext(),ScheduleActivity.class);

        intent.putExtra("roundschedule", adapterPosition+1);


        Log.d("Races calendar",retourCalendrier.get(adapterPosition).getRound());



        Log.d("position", String.valueOf(adapterPosition));



        startActivity(intent);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;

    }



    void callAPI2(){



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ergast.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        ScheduleSeason service = retrofit.create(ScheduleSeason.class);
        service.listRepos("f1").enqueue(new Callback<ScheduleRepository>() {
            @Override

            public void onResponse(Call<ScheduleRepository> call, Response<ScheduleRepository> response) {
                ScheduleRepository repository = response.body();

 //affichage console*/
                //Log.d("RESPONSE API2", repository.toString()); //affichage console

                if (repository != null){

                    retourCalendrier= repository.getMRData().getRaceTable().getRaces();
                    List<Races> Races = repository.getMRData().getRaceTable().getRaces();
                    MyScheduleData[] MyScheduleData = new MyScheduleData[Races.size()];

                   for(int i=0;i<Races.size();i++){


                   MyScheduleData[i] = new MyScheduleData( Races.get(i).getDate(),
                                                                Races.get(i).getTime(),
                                                                Races.get(i).getRound(),
                                                                Races.get(i).getCircuit().getLocation().getLocality(),
                                                                Races.get(i).getCircuit().getLocation().getCountry(),
                                                                Races.get(i).getCircuit().getCircuitName());
                   }



                    adapter.add(MyScheduleData);
                    adapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(Call<ScheduleRepository> call, Throwable t) {

                Log.e("err",t.toString());

            }
        });


    }



}