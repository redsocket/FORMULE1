package com.example.f1_app.ui.api;

import com.example.f1_app.ui.DriversInformations.DriversRepository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DriverInformation {

    @GET("{user}.json")
    Call<DriversRepository> listRepos(@Path("user") String user);


}
