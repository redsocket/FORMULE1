package com.example.f1_app.ui.RecyclerViewCircuitsInformations;

import com.google.gson.annotations.SerializedName;

public class MyListDataCircuitsInformations {

    private String circuitName;
    private String country;
    private String lat;
    private String locality;
    @SerializedName("long")
    private String longueur;


    public MyListDataCircuitsInformations(String circuitName,String lat,
                                          String longueur, String locality,
                                          String country)
    {

        this.circuitName = circuitName;
        this.country = country;
        this.lat = lat;
        this.locality=locality;
        this.longueur=longueur;
    }

    public String getCircuitName() {
        return circuitName;
    }

    public void setCircuitName(String circuitName) {
        this.circuitName = circuitName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getLongueur() {
        return longueur;
    }

    public void setLongueur(String longueur) {
        this.longueur = longueur;
    }
}
