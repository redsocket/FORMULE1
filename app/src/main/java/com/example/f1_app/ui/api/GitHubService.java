package com.example.f1_app.ui.api;



import com.example.f1_app.ui.classe_pilotes.CodeRepository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitHubService {
    @GET("{user}/drivers.json")
    Call<CodeRepository> listRepos(@Path("user") String user);

}
