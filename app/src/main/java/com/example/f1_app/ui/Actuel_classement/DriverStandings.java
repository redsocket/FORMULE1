package com.example.f1_app.ui.Actuel_classement;

import androidx.annotation.NonNull;

import java.util.List;

public class DriverStandings {

    private String position ;
    private String positionText ;
    private String points ;
    private String wins ;
    private Driver Driver;
    private List< Constructors> Constructors;


    @NonNull
    @Override
    public String toString() {
        return "DriverStandings{" +
                "position='" + position + '\n' +
                ", points='" + points + '\n' +
                ", wins='" + wins + '\n' +
                ", Driver=" + Driver +'\n'+
                ", Constructors=" + Constructors +'\n'+
                '}';
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionText() {
        return positionText;
    }

    public void setPositionText(String positionText) {
        this.positionText = positionText;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getWins() {
        return wins;
    }

    public void setWins(String wins) {
        this.wins = wins;
    }

    public com.example.f1_app.ui.Actuel_classement.Driver getDriver() {
        return Driver;
    }

    public void setDriver(com.example.f1_app.ui.Actuel_classement.Driver driver) {
        Driver = driver;
    }

    public List<com.example.f1_app.ui.Actuel_classement.Constructors> getConstructors() {
        return Constructors;
    }

    public void setConstructors(List<com.example.f1_app.ui.Actuel_classement.Constructors> constructors) {
        Constructors = constructors;
    }
}
