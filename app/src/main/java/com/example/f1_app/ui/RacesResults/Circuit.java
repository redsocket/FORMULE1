package com.example.f1_app.ui.RacesResults;

public class Circuit {

    private String circuitId;
    private String url;
    private String circuitName;
    private Location Location;

    @Override
    public String toString() {
        return "Circuit{" +
                "circuitId='" + circuitId + '\'' +
                ", url='" + url + '\'' +
                ", circuitName='" + circuitName + '\'' +
                ", Location=" + Location +
                '}';
    }

    public String getCircuitId() {
        return circuitId;
    }

    public void setCircuitId(String circuitId) {
        this.circuitId = circuitId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCircuitName() {
        return circuitName;
    }

    public void setCircuitName(String circuitName) {
        this.circuitName = circuitName;
    }

    public com.example.f1_app.ui.RacesResults.Location getLocation() {
        return Location;
    }

    public void setLocation(com.example.f1_app.ui.RacesResults.Location location) {
        Location = location;
    }
}
