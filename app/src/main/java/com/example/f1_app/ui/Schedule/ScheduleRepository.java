package com.example.f1_app.ui.Schedule;

public class ScheduleRepository {

    private MRData MRData;

    @Override
    public String toString() {
        return "ScheduleRepository{" +
                "MRDate=" + MRData +
                '}';
    }

    public com.example.f1_app.ui.Schedule.MRData getMRData() {
        return MRData;
    }
}
