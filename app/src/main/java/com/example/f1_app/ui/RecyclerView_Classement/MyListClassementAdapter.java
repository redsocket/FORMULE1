package com.example.f1_app.ui.RecyclerView_Classement;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.R;
import com.squareup.picasso.Picasso;


public class MyListClassementAdapter extends RecyclerView.Adapter<MyListClassementAdapter.ViewHolder>{
    private MyClassementData[] listdata;

    private int taille;

    // RecyclerView recyclerView;
    public MyListClassementAdapter(MyClassementData[] listdata) {

        if(this.listdata !=null) {
            this.listdata = listdata;
            this.taille = listdata.length;
        }
        else{this.taille=0;}

    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_items_classement, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final MyClassementData MyClassementData = listdata[position];
        holder.textView.setText(listdata[position].getGivenName());
        holder.textView1.setText(listdata[position].getFamilyName());
        holder.textView3.setText(listdata[position].getPosition());
        holder.textView4.setText(listdata[position].getPoints());
        holder.textView5.setText(listdata[position].getVictoires());


        if(listdata[position].getName().equals("Red Bull")){
            Picasso.get().load("https://static.wikia.nocookie.net/fanmade_f1/images/9/98/Red_Bull_Logo.png/revision/latest?cb=20200512022358").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Mercedes")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Mercedes_AMG_Petronas_F1_Logo.svg/2560px-Mercedes_AMG_Petronas_F1_Logo.svg.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("McLaren")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/b/ba/McLaren_Racing_logo_2021.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Ferrari")){
            Picasso.get().load("http://assets.stickpng.com/images/580b585b2edbce24c47b2c52.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("AlphaTauri")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/thumb/b/b0/Scuderia_AlphaTauri.svg/1200px-Scuderia_AlphaTauri.svg.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Alpine F1 Team")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Alpine_F1_Team_Logo.svg/2233px-Alpine_F1_Team_Logo.svg.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Aston Martin")){
            Picasso.get().load("https://www.formula1.com/content/fom-website/en/teams/Aston-Martin/_jcr_content/logo.img.png/1610642928926.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Williams")){
            Picasso.get().load("https://www.mpadeco.com/thumb.php?zc=3&f=0&src=/sites/mpadeco/files/products/6e4a433b88759efd5afc3a08ba6946ac.png&fl=none&w=500&h=500").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Alfa Romeo")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/it/5/55/Logo_Alfa_Romeo_Racing.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Haas F1 Team")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Uralkali_Haas_F1_Team_Logo.svg/1200px-Uralkali_Haas_F1_Team_Logo.svg.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Renault")){
            Picasso.get().load("https://risibank.fr/cache/stickers/d1955/195519-full.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Racing Point")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/BWT_Racing_Point_Logo.svg/1200px-BWT_Racing_Point_Logo.svg.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Toro Rosso")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/thumb/4/4d/Scuderia_Toro_Rosso_2018-2019_Logo.svg/1280px-Scuderia_Toro_Rosso_2018-2019_Logo.svg.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Sauber")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/thumb/f/fb/Logo_Alfa_Romeo_Sauber_F1_Team.png/1200px-Logo_Alfa_Romeo_Sauber_F1_Team.png").into(holder.imageView3);
        }
        else if(listdata[position].getName().equals("Force India")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/thumb/e/e8/Force_India.svg/1200px-Force_India.svg.png").into(holder.imageView3);
        }






        if(listdata[position].getNationality().equals("Spanish")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/750px-Flag_of_Spain.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Finnish")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Finland.svg/800px-Flag_of_Finland.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("French")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/langfr-1280px-Flag_of_France.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Italian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/800px-Flag_of_Italy.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("British")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/800px-Flag_of_the_United_Kingdom.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Polish")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Flag_of_Poland.svg/120px-Flag_of_Poland.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Canadian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/langfr-1920px-Flag_of_Canada_%28Pantone%29.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Monegasque")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Flag_of_Monaco.svg/750px-Flag_of_Monaco.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Russian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/800px-Flag_of_Russia.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Mexican")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/800px-Flag_of_Mexico.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Australian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/1920px-Flag_of_Australia_%28converted%29.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("German")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/800px-Flag_of_Germany.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Japanese")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Flag_of_Japan.svg/800px-Flag_of_Japan.svg.png").into(holder.imageView13);
        }
        else if(listdata[position].getNationality().equals("Dutch")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/800px-Flag_of_the_Netherlands.svg.png").into(holder.imageView13);
        }






        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(view.getContext(),"click on item: "+myListData.getDescription(),Toast.LENGTH_LONG).show();
            }
        });

    }

    public void add( MyClassementData[] classementData){

        this.listdata=classementData;
        this.taille=classementData.length;
    }

    @Override
    public int getItemCount() {
        return this.taille;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
       // public ImageView imageView;

        public TextView textView;
        public TextView textView1;
        public TextView textView3;
        public TextView textView4;
        public TextView textView5;
        public ImageView imageView3;
        public ImageView imageView13;

        public RelativeLayout relativeLayout;
        public ViewHolder(View itemView) {
            super(itemView);

            this.textView = (TextView) itemView.findViewById(R.id.givenName);
            this.textView1 = (TextView) itemView.findViewById(R.id.familyName);
            this.textView3 = (TextView) itemView.findViewById(R.id.position);
            this.textView4 = (TextView) itemView.findViewById(R.id.points);
            this.imageView3 = (ImageView) itemView.findViewById(R.id.imageView3);
            this.imageView13 = (ImageView) itemView.findViewById(R.id.imageView13);
            this.textView5 = (TextView) itemView.findViewById(R.id.victoires);


            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.ResultView);
        }
    }
}