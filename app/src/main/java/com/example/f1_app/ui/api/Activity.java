package com.example.f1_app.ui.api;


import com.example.f1_app.ui.RacesResults.ResultsRepository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Activity
{
    @GET("{user}/results.json")
    Call<ResultsRepository> listRepos(@Path("user") int user);

}
