package com.example.f1_app.ui.class_circuits;
import androidx.annotation.NonNull;

public class Circuits {

    private String circuitId;
    private String url;
    private String circuitName;

    private Location Location;

    @NonNull
    @Override

    public String toString() {

        return "\n {" +
                "Nom du circuit :" + circuitName + "\n" +
                "Location : \n" + Location ;

    }

    public String getCircuitName() {

        return circuitName;
    }

    public com.example.f1_app.ui.class_circuits.Location getLocation() {

        return Location;
    }
}