package com.example.f1_app.ui.class_circuits;

import androidx.annotation.NonNull;
import java.util.List;

public class CircuitTable{

    private String season;
    private List<Circuits> Circuits;

    @NonNull
    @Override
    public String toString(){
        return "Saison :" + season +"\n" +
                Circuits;
    }

    public String getSeason(){return season;}
    public void setSeason(String season) {this.season = season;}


    public List<com.example.f1_app.ui.class_circuits.Circuits> getCircuits() {
        return Circuits;
    }

    public void setCircuits(List<com.example.f1_app.ui.class_circuits.Circuits> circuits) {
        Circuits = circuits;
    }
}
