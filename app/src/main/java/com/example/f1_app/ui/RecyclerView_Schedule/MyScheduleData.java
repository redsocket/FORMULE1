package com.example.f1_app.ui.RecyclerView_Schedule;

public class MyScheduleData {

    private String date;
    private String time;
    private String round;

    private String Locality;
    private String country;
    private String circuitName;

    public MyScheduleData(String date, String time, String round, String locality, String country, String circuitName) {
        this.date = date;
        this.time = time;
        this.round = round;
        this.Locality = locality;
        this.country = country;
        this.circuitName = circuitName;
    }



    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }

    public String getLocality() {
        return Locality;
    }

    public void setLocality(String locality) {
        Locality = locality;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCircuitName() {
        return circuitName;
    }

    public void setCircuitName(String circuitName) {
        this.circuitName = circuitName;
    }
}
