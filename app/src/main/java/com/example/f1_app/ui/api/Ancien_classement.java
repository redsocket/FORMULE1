package com.example.f1_app.ui.api;

import com.example.f1_app.ui.Actuel_classement.ClassementRepository;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Ancien_classement {
    @GET("{user}/21/driverStandings.json")
    Call<ClassementRepository> listRepos(@Path("user") String user);


}
