package com.example.f1_app.ui.RecyclerViewCircuits;

import com.google.gson.annotations.SerializedName;

public class MyCircuitData {

    private String circuitName;
    private String country;
    private String lat;
    private String locality;
    @SerializedName("long")
    private String longueur;



    public MyCircuitData(String circuitName, String country, String longueur, String lat, String locality)
    {

        this.circuitName = circuitName;
        this.country = country;
        this.lat = lat;
        this.locality=locality;
        this.longueur=longueur;
    }

    public String getCircuitName() {
        return circuitName;
    }

    public String getLat() {
        return lat;
    }

    public String getLocality() {
        return locality;
    }

    public String getLongueur() {
        return longueur;
    }

    public void setCircuitName(String circuitName) {
        this.circuitName = circuitName;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
