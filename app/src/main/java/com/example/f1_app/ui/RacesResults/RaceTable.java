package com.example.f1_app.ui.RacesResults;

import java.util.List;

public class RaceTable {

    private List<Races> Races;
    private String season;
    private String round;


    @Override
    public String toString() {
        return "RaceTable{" +
                "Races=" + Races +
                ", season='" + season + '\'' +
                ", round='" + round + '\'' +

                '}';
    }



    public List<com.example.f1_app.ui.RacesResults.Races> getRaces() {
        return Races;
    }

    public void setRaces(List<com.example.f1_app.ui.RacesResults.Races> races) {
        Races = races;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getRound() {
        return round;
    }

    public void setRound(String round) {
        this.round = round;
    }
}
