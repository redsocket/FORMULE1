package com.example.f1_app.ui.Schedule;

import java.util.List;

public class RaceTable {

    private String season;
    private List<Races> Races;

    @Override
    public String toString() {
        return "RaceTable{" +
                "season='" + season + '\'' +
                ", Races=" + Races +
                '}';
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public List<com.example.f1_app.ui.Schedule.Races> getRaces() {
        return Races;
    }

    public void setRaces(List<com.example.f1_app.ui.Schedule.Races> races) {
        Races = races;
    }
}
