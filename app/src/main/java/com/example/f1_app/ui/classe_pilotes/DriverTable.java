package com.example.f1_app.ui.classe_pilotes;

import androidx.annotation.NonNull;

import java.util.List;

public class DriverTable {

    private String season ;
    private List<Drivers> Drivers;


    @NonNull
    @Override
    public String toString() {
        return ""+Drivers;
    }

    public String getSeason() { return season;}

    public void setSeason(String season) { this.season = season;}

    public List<com.example.f1_app.ui.classe_pilotes.Drivers> getDrivers() {
        return Drivers;
    }
}
