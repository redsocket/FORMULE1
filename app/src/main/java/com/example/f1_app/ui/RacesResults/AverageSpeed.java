package com.example.f1_app.ui.RacesResults;

public class AverageSpeed {

    private String units;
    private String speed;

    @Override
    public String toString() {
        return "AverageSpeed{" +
                "units='" + units + '\'' +
                ", speed='" + speed + '\'' +
                '}';
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }
}
