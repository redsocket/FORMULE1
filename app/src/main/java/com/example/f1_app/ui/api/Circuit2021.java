package com.example.f1_app.ui.api;


import com.example.f1_app.ui.class_circuits.circuitRepository;
import com.example.f1_app.ui.classe_pilotes.CodeRepository;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.Call;

public interface Circuit2021 {

    @GET("{user}/circuits.json")
    Call<circuitRepository> listRepos(@Path("user") String user);

}
