package com.example.f1_app.ui.Schedule;

import androidx.annotation.NonNull;

import com.example.f1_app.ui.Actuel_classement.StandingsTable;

public class MRData {


    private String series;
    private String total;
    private RaceTable RaceTable;


    @NonNull
    @Override
    public String toString() {
        return "MRData{" +
                "series='" + series + '\n' +
                "total=" + total +'\n'+
                " "+ RaceTable+
                '}';
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }


    public com.example.f1_app.ui.Schedule.RaceTable getRaceTable() {
        return RaceTable;
    }
}
