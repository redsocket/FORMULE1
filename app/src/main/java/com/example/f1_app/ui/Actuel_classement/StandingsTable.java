package com.example.f1_app.ui.Actuel_classement;

import androidx.annotation.NonNull;

import java.util.List;

public class StandingsTable {

    private String season;
    private List<StandingsLists> StandingsLists;



    @NonNull
    @Override
    public String toString() {
        return ""+StandingsLists;
    }

    public String getSeason() { return season;}

    public void setSeason(String season) {
        this.season = season;
    }

    public List<com.example.f1_app.ui.Actuel_classement.StandingsLists> getStandingsLists() {
        return StandingsLists;
    }


    public void setStandingsLists(List<com.example.f1_app.ui.Actuel_classement.StandingsLists> standingsLists) {
        StandingsLists = standingsLists;
    }
}
