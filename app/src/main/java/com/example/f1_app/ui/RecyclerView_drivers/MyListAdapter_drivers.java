package com.example.f1_app.ui.RecyclerView_drivers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.R;
import com.squareup.picasso.Picasso;


public class MyListAdapter_drivers extends RecyclerView.Adapter<MyListAdapter_drivers.ViewHolder>{
    private MyListData_pilotes[] listdata;

    // RecyclerView recyclerView;
    private int size;
    private OnDriversListener onDriversListener;

    public MyListAdapter_drivers(MyListData_pilotes[] listdata, OnDriversListener onDriversListener)
    {
        if(this.listdata !=null){

        this.listdata = listdata;
        this.size = listdata.length;

        }
        else{this.size=0;}

        this.onDriversListener=onDriversListener;
    }
    @Override
    public MyListAdapter_drivers.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_items_drivers, parent, false);

        MyListAdapter_drivers.ViewHolder viewHolder = new MyListAdapter_drivers.ViewHolder(listItem,this.onDriversListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final MyListData_pilotes MyListData_pilotes = listdata[position];
        holder.textView.setText(listdata[position].getGivenName());
        holder.textView1.setText(listdata[position].getFamilyName());
        holder.textView2.setText(listdata[position].getNationality());
        holder.textView3.setText(listdata[position].getDateOfBirth());
        holder.textView4.setText(listdata[position].getPermanentNumber());

        if(listdata[position].getFamilyName().equals("Alonso")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/F/FERALO01_Fernando_Alonso/feralo01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Bottas")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/V/VALBOT01_Valtteri_Bottas/valbot01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Gasly")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/P/PIEGAS01_Pierre_Gasly/piegas01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Giovinazzi")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/A/ANTGIO01_Antonio_Giovinazzi/antgio01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Hamilton")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/L/LEWHAM01_Lewis_Hamilton/lewham01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Kubica")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/R/ROBKUB01_Robert_Kubica/robkub01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Latifi")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/N/NICLAF01_Nicholas_Latifi/niclaf01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Leclerc")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/C/CHALEC01_Charles_Leclerc/chalec01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Mazepin")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/N/NIKMAZ01_Nikita_Mazepin/nikmaz01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Norris")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/L/LANNOR01_Lando_Norris/lannor01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Ocon")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/E/ESTOCO01_Esteban_Ocon/estoco01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Pérez")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/S/SERPER01_Sergio_Perez/serper01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Räikkönen")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/K/KIMRAI01_Kimi_R%C3%A4ikk%C3%B6nen/kimrai01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Ricciardo")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/D/DANRIC01_Daniel_Ricciardo/danric01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Russell")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/G/GEORUS01_George_Russell/georus01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Sainz")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/C/CARSAI01_Carlos_Sainz/carsai01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Schumacher")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/M/MICSCH02_Mick_Schumacher/micsch02.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Verstappen")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/M/MAXVER01_Max_Verstappen/maxver01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Tsunoda")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/Y/YUKTSU01_Yuki_Tsunoda/yuktsu01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Vettel")) {
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/S/SEBVET01_Sebastian_Vettel/sebvet01.png.transform/2col/image.png").into(holder.imageView2);
        }
        else if(listdata[position].getFamilyName().equals("Stroll")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/L/LANSTR01_Lance_Stroll/lanstr01.png.transform/2col/image.png").into(holder.imageView2);
        }


        if(listdata[position].getNationality().equals("Spanish")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/750px-Flag_of_Spain.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Finnish")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Finland.svg/800px-Flag_of_Finland.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("French")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/langfr-1280px-Flag_of_France.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Italian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/800px-Flag_of_Italy.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("British")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/800px-Flag_of_the_United_Kingdom.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Polish")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Flag_of_Poland.svg/120px-Flag_of_Poland.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Canadian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/langfr-1920px-Flag_of_Canada_%28Pantone%29.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Monegasque")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Flag_of_Monaco.svg/750px-Flag_of_Monaco.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Russian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/800px-Flag_of_Russia.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Mexican")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/800px-Flag_of_Mexico.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Australian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/1920px-Flag_of_Australia_%28converted%29.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("German")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/800px-Flag_of_Germany.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Japanese")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Flag_of_Japan.svg/800px-Flag_of_Japan.svg.png").into(holder.imageView);
        }
        else if(listdata[position].getNationality().equals("Dutch")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/800px-Flag_of_the_Netherlands.svg.png").into(holder.imageView);
        }


    }

    public void add(MyListData_pilotes[] data_pilotes){

        this.listdata=data_pilotes;
        this.size=data_pilotes.length;
    }


    @Override
    public int getItemCount() {
        return this.size;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {

        public TextView textView;
        public TextView textView1;
        public TextView textView2;
        public TextView textView3;
        public TextView textView4;
       public RelativeLayout relativeLayout;
        public ImageView imageView2;
        public ImageView imageView;

        private OnDriversListener onDriversListener;

        public ViewHolder(View itemView, OnDriversListener onDriversListener) {
            super(itemView);

            this.textView=(TextView) itemView.findViewById(R.id.givenName);
            this.textView1=(TextView) itemView.findViewById(R.id.familyName);
            this.textView2=(TextView) itemView.findViewById(R.id.nationality);
            this.textView3=(TextView) itemView.findViewById(R.id.dateOfBirthday);
            this.textView4=(TextView) itemView.findViewById(R.id.permanentNumber);
            this.imageView2 = (ImageView) itemView.findViewById(R.id.imageView2);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);

            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.driversView);
            itemView.setOnClickListener(this);
            this.onDriversListener=onDriversListener;
        }


        @Override
        public void onClick(View view)
        {
            this.onDriversListener.OnDriversClick(getAdapterPosition());
        }
    }

    public interface OnDriversListener{

        void OnDriversClick(int adapterPosition);
    }

}