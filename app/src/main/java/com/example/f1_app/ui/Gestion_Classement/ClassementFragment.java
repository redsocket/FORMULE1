package com.example.f1_app.ui.Gestion_Classement;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.f1_app.databinding.FragmentClassementBinding;
import com.example.f1_app.ui.Gestion_Classement.FragmentClassement.SectionsPagerAdapterClassement;
import com.google.android.material.tabs.TabLayout;


public class ClassementFragment extends Fragment{


    private FragmentClassementBinding binding;




    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        binding = FragmentClassementBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        SectionsPagerAdapterClassement SectionsPagerAdapterClassement = new SectionsPagerAdapterClassement(getContext(), getChildFragmentManager());
        Log.d("RESPONSE tab", "position change");
        ViewPager viewPager = binding.viewPager;
        viewPager.setAdapter(SectionsPagerAdapterClassement);
        TabLayout tabs = binding.tabs;
        tabs.setupWithViewPager(viewPager);
        //FloatingActionButton fab = binding.fab;
        TabLayout fab = binding.tabs;

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });


        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}