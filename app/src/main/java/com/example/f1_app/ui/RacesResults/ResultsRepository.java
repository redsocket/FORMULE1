package com.example.f1_app.ui.RacesResults;

public class ResultsRepository {
    private MRData MRData;

    @Override
    public String toString() {
       return MRData.toString();
    }

    public com.example.f1_app.ui.RacesResults.MRData getMRData() {
        return MRData;
    }

    public void setMRData(com.example.f1_app.ui.RacesResults.MRData MRData) {
        this.MRData = MRData;
    }
}
