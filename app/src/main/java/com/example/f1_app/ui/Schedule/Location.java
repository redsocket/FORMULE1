package com.example.f1_app.ui.Schedule;

public class Location {


    private String Lat;
    private String locality;
    private String country;

    @Override
    public String toString() {
        return "Location{" +
                "Lat='" + Lat + '\'' +
                ", Locality='" + locality + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        locality = locality;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
