package com.example.f1_app.ui.classe_pilotes;

import androidx.annotation.NonNull;

public class MRData {

    private String total;

    public String series;

    private DriverTable DriverTable;

    @NonNull
    @Override
    public String toString() {
        return  "MRData {"+ "\n" +
                "series="  + total +  " \n" +
                "DriverTable=" + DriverTable +
                "}";


    }

    public String getSeries() {
        return series;
    }

    public String getTotal(){return total;}

    public com.example.f1_app.ui.classe_pilotes.DriverTable getDriverTable(){
        return DriverTable;
    }

    public void setDriverTable(com.example.f1_app.ui.classe_pilotes.DriverTable driverTable) {
        DriverTable = driverTable;
    }
}
