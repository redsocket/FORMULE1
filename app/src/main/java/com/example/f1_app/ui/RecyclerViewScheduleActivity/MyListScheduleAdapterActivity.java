package com.example.f1_app.ui.RecyclerViewScheduleActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.R;
import com.squareup.picasso.Picasso;

public class MyListScheduleAdapterActivity extends RecyclerView.Adapter<MyListScheduleAdapterActivity.ViewHolder> {


    private MyScheduleDataActivity[] listdata;
    private int size =0;

    public MyListScheduleAdapterActivity(MyScheduleDataActivity[] listdata){

        if(this.listdata !=null){
            this.listdata = listdata;
            this.size=listdata.length;
        }

        else{this.size=0;}

    }

    public MyListScheduleAdapterActivity.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem=layoutInflater.inflate(R.layout.list_items_shedule_activity, parent, false);

        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }


    public void onBindViewHolder(ViewHolder holder, int position){

        final MyScheduleDataActivity MyScheduleDataActivity = listdata[position];

       holder.textView1.setText(listdata[position].getUnits());
       holder.textView2.setText(listdata[position].getSpeed());
       holder.textView4.setText(listdata[position].getCircuitName());

       // holder.textView5.setText(listdata[position].getLocality());
       // holder.textView6.setText(listdata[position].getCountry());

       holder.textView7.setText(listdata[position].getName());
       holder.textView8.setText(listdata[position].getNationality());

       holder.textView9.setText(listdata[position].getGivenName());
       holder.textView10.setText(listdata[position].getFamilyName());

       holder.textView11.setText(listdata[position].getRank());
       holder.textView12.setText(listdata[position].getLap());

       holder.textView13.setText((CharSequence) listdata[position].getTime());
       //holder.textView14.setText(listdata[position].getDate());
       //holder.textView15.setText(listdata[position].getRaceName());
       holder.textView16.setText(listdata[position].getNumber());
       holder.textView17.setText(listdata[position].getPosition());

       holder.textView18.setText(listdata[position].getPoints());
       holder.textView19.setText(listdata[position].getGrid());
       holder.textView20.setText(listdata[position].getLaps());

        if(listdata[position].getFamilyName().equals("Alonso")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/F/FERALO01_Fernando_Alonso/feralo01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Bottas")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/V/VALBOT01_Valtteri_Bottas/valbot01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Gasly")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/P/PIEGAS01_Pierre_Gasly/piegas01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Giovinazzi")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/A/ANTGIO01_Antonio_Giovinazzi/antgio01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Hamilton")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/L/LEWHAM01_Lewis_Hamilton/lewham01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Kubica")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/R/ROBKUB01_Robert_Kubica/robkub01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Latifi")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/N/NICLAF01_Nicholas_Latifi/niclaf01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Leclerc")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/C/CHALEC01_Charles_Leclerc/chalec01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Mazepin")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/N/NIKMAZ01_Nikita_Mazepin/nikmaz01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Norris")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/L/LANNOR01_Lando_Norris/lannor01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Ocon")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/E/ESTOCO01_Esteban_Ocon/estoco01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Pérez")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/S/SERPER01_Sergio_Perez/serper01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Räikkönen")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/K/KIMRAI01_Kimi_R%C3%A4ikk%C3%B6nen/kimrai01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Ricciardo")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/D/DANRIC01_Daniel_Ricciardo/danric01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Russell")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/G/GEORUS01_George_Russell/georus01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Sainz")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/C/CARSAI01_Carlos_Sainz/carsai01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Schumacher")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/M/MICSCH02_Mick_Schumacher/micsch02.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Verstappen")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/M/MAXVER01_Max_Verstappen/maxver01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Tsunoda")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/Y/YUKTSU01_Yuki_Tsunoda/yuktsu01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Vettel")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/S/SEBVET01_Sebastian_Vettel/sebvet01.png.transform/2col/image.png").into(holder.imageView14);
        }
        else if(listdata[position].getFamilyName().equals("Stroll")){
            Picasso.get().load("https://www.formula1.com/content/dam/fom-website/drivers/L/LANSTR01_Lance_Stroll/lanstr01.png.transform/2col/image.png").into(holder.imageView14);
        }


        if(listdata[position].getNationality().equals("Spanish")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/750px-Flag_of_Spain.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Finnish")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Flag_of_Finland.svg/800px-Flag_of_Finland.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("French")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/langfr-1280px-Flag_of_France.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Italian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/0/03/Flag_of_Italy.svg/800px-Flag_of_Italy.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("British")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Flag_of_the_United_Kingdom.svg/800px-Flag_of_the_United_Kingdom.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Polish")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/1/12/Flag_of_Poland.svg/120px-Flag_of_Poland.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Canadian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/langfr-1920px-Flag_of_Canada_%28Pantone%29.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Monegasque")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/Flag_of_Monaco.svg/750px-Flag_of_Monaco.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Russian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Flag_of_Russia.svg/800px-Flag_of_Russia.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Mexican")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/fc/Flag_of_Mexico.svg/800px-Flag_of_Mexico.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Australian")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Flag_of_Australia_%28converted%29.svg/1920px-Flag_of_Australia_%28converted%29.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("German")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/800px-Flag_of_Germany.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Japanese")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Flag_of_Japan.svg/800px-Flag_of_Japan.svg.png").into(holder.imageView15);
        }
        else if(listdata[position].getNationality().equals("Dutch")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flag_of_the_Netherlands.svg/800px-Flag_of_the_Netherlands.svg.png").into(holder.imageView15);
        }


        if(listdata[position].getName().equals("Red Bull")){
            Picasso.get().load("https://static.wikia.nocookie.net/fanmade_f1/images/9/98/Red_Bull_Logo.png/revision/latest?cb=20200512022358").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Mercedes")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/fb/Mercedes_AMG_Petronas_F1_Logo.svg/2560px-Mercedes_AMG_Petronas_F1_Logo.svg.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("McLaren")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/b/ba/McLaren_Racing_logo_2021.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Ferrari")){
            Picasso.get().load("http://assets.stickpng.com/images/580b585b2edbce24c47b2c52.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("AlphaTauri")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/thumb/b/b0/Scuderia_AlphaTauri.svg/1200px-Scuderia_AlphaTauri.svg.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Alpine F1 Team")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Alpine_F1_Team_Logo.svg/2233px-Alpine_F1_Team_Logo.svg.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Aston Martin")){
            Picasso.get().load("https://www.formula1.com/content/fom-website/en/teams/Aston-Martin/_jcr_content/logo.img.png/1610642928926.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Williams")){
            Picasso.get().load("https://www.mpadeco.com/thumb.php?zc=3&f=0&src=/sites/mpadeco/files/products/6e4a433b88759efd5afc3a08ba6946ac.png&fl=none&w=500&h=500").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Alfa Romeo")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/it/5/55/Logo_Alfa_Romeo_Racing.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Haas F1 Team")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Uralkali_Haas_F1_Team_Logo.svg/1200px-Uralkali_Haas_F1_Team_Logo.svg.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Renault")){
            Picasso.get().load("https://risibank.fr/cache/stickers/d1955/195519-full.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Racing Point")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/BWT_Racing_Point_Logo.svg/1200px-BWT_Racing_Point_Logo.svg.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Toro Rosso")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/thumb/4/4d/Scuderia_Toro_Rosso_2018-2019_Logo.svg/1280px-Scuderia_Toro_Rosso_2018-2019_Logo.svg.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Sauber")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/thumb/f/fb/Logo_Alfa_Romeo_Sauber_F1_Team.png/1200px-Logo_Alfa_Romeo_Sauber_F1_Team.png").into(holder.imageView16);
        }
        else if(listdata[position].getName().equals("Force India")){
            Picasso.get().load("https://upload.wikimedia.org/wikipedia/fr/thumb/e/e8/Force_India.svg/1200px-Force_India.svg.png").into(holder.imageView16);
        }


    }

    public int getItemCount(){

        return this.size;
    }

    public void add3( MyScheduleDataActivity[] classementData){

        this.listdata=classementData;
        this.size=classementData.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout relativeLayout;

       // public TextView textView;
        public TextView textView1;
        public TextView textView2;
        // public TextView textView3;
        public TextView textView4;
       // public TextView textView5;
       // public TextView textView6;
        public TextView textView7;
        public TextView textView8;
        public TextView textView9;
        public TextView textView10;
        public TextView textView11;
        public TextView textView12;
        public TextView textView13;
       // public TextView textView14;
       // public TextView textView15;
        public TextView textView16;
        public TextView textView17;
        public TextView textView18;
        public TextView textView19;
        public TextView textView20;

        public ImageView imageView14;
        public ImageView imageView15;
        public ImageView imageView16;




        public ViewHolder(View itemView) {

            super(itemView);

            this.textView1=(TextView) itemView.findViewById(R.id.units);
            this.textView2=(TextView) itemView.findViewById(R.id.speed);
           // this.textView3=(TextView) itemView.findViewById(R.id.circuitId);
            this.textView4=(TextView) itemView.findViewById(R.id.circuitname);
           // this.textView5=(TextView) itemView.findViewById(R.id.locality);
            //this.textView6=(TextView) itemView.findViewById(R.id.country);
            this.textView7=(TextView) itemView.findViewById(R.id.name);
            this.textView8=(TextView) itemView.findViewById(R.id.nationality);
            this.textView9=(TextView) itemView.findViewById(R.id.givenName);
            this.textView10=(TextView) itemView.findViewById(R.id.familyName);
            this.textView11=(TextView) itemView.findViewById(R.id.rank);
            this.textView12=(TextView) itemView.findViewById(R.id.lap);
            this.textView13=(TextView) itemView.findViewById(R.id.time);
            // this.textView14=(TextView) itemView.findViewById(R.id.date);
            //this.textView15=(TextView) itemView.findViewById(R.id.raceName);
            this.textView16=(TextView) itemView.findViewById(R.id.number);
            this.textView17=(TextView) itemView.findViewById(R.id.position);
            this.textView18=(TextView) itemView.findViewById(R.id.points);
            this.textView19=(TextView) itemView.findViewById(R.id.grid);
            this.textView20=(TextView) itemView.findViewById(R.id.laps);
            this.imageView14 = (ImageView) itemView.findViewById(R.id.imageView14);
            this.imageView15 = (ImageView) itemView.findViewById(R.id.imageView15);
            this.imageView16 = (ImageView) itemView.findViewById(R.id.imageView16);

            //relativeLayout = (RelativeLayout)itemView.findViewById(R.id.ResultView);


        }

    }
}
