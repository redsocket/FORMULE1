package com.example.f1_app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.f1_app.ui.CircuitsInformations.Circuits;
import com.example.f1_app.ui.CircuitsInformations.CircuitsRepository;
import com.example.f1_app.ui.RecyclerViewCircuitsInformations.MyListAdapterCircuitsInformations;
import com.example.f1_app.ui.RecyclerViewCircuitsInformations.MyListDataCircuitsInformations;
import com.example.f1_app.ui.api.CircuitsInformations;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CircuitsActivity extends AppCompatActivity {


    private Button boutonclose;

    RecyclerView recyclerView;
    MyListAdapterCircuitsInformations adapter;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();


        int adapterPosition = intent.getIntExtra("Circuits", 1);

        switch (adapterPosition) {

            case 1:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("americas");
                break;
            case 2:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("bahrain");

                break;
            case 3:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("BAK");

                break;
            case 4:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("catalunya");

                break;
            case 5:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("hungaroring");


                break;
            case 6:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("imola");

                break;
            case 7:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("interlagos");

                break;
            case 8:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("istanbul");


                break;
            case 9:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("jeddah");

                break;
            case 10:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("losail");

                break;
            case 11:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("monaco");

                break;
            case 12:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("monza");

                break;
            case 13:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("portimao");

                break;
            case 14:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("red_bull_ring");

                break;
            case 15:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("ricard");

                break;
            case 16:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("rodriguez");

                break;
            case 17:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("silverstone");

                break;
            case 18:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("sochi");

                break;
            case 19:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("spa");

                break;
            case 20:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("yas_marina");

                break;
            case 21:
                setContentView((R.layout.activity_circuits));
                callAPI_circuits_activity("zandvoort");

                break;


        }

        boutonclose = (Button) findViewById(R.id.circuitsActivity);

        boutonclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });
    }

        void callAPI_circuits_activity(String circuitsId){

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://ergast.com/api/f1/circuits/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            CircuitsInformations service = retrofit.create(CircuitsInformations.class);
            service.listRepos(circuitsId).enqueue(new Callback<CircuitsRepository>() {
                @Override
                public void onResponse(Call<CircuitsRepository> call, Response<CircuitsRepository> response) {
                    CircuitsRepository repository = response.body();

                    if(repository !=null)
                    {

                        List<Circuits> Circuits = repository.getMRData().getCircuitTable().getCircuits();
                        MyListDataCircuitsInformations[] MyListDataCircuitsInformations= new MyListDataCircuitsInformations[Circuits.size()];

                        for (int i = 0; i < Circuits.size(); i++) {

                            MyListDataCircuitsInformations[i] = new MyListDataCircuitsInformations(
                                    Circuits.get(i).getCircuitName(),
                                    Circuits.get(i).getLocation().getLat(),
                                    Circuits.get(i).getLocation().getLongueur(),
                                    Circuits.get(i).getLocation().getLocality(),
                                    Circuits.get(i).getLocation().getCountry());
                        }

                        recyclerView = findViewById(R.id.activity_circuits);
                        MyListAdapterCircuitsInformations adapter = new MyListAdapterCircuitsInformations(MyListDataCircuitsInformations);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                        recyclerView.setAdapter(adapter);


                        adapter.add(MyListDataCircuitsInformations);
                        adapter.notifyDataSetChanged();

                    }




                }

                @Override
                public void onFailure(Call<CircuitsRepository> call, Throwable t) {

                }
            });
        }








    }




